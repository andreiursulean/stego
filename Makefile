SUBDIRS = $(shell find src/ -type d)
PROFILE = Release

BASE_FLAGS = -c --std=c++17 -MMD -Isrc -Isrc/libjpeg-turbo -Isrc/libjpeg-turbo/build# -Isrc/libjpeg  $(addprefix -I, $(SUBDIRS)) 

ifeq ($(PROFILE), Release)
CXXFLAGS = $(BASE_FLAGS) -O3 
else ifeq ($(PROFILE), Debug)
CXXFLAGS = $(BASE_FLAGS) -Og -g
endif

CPP_FILES = $(shell find src/ -name "*.cpp")
OBJ_FILES = $(CPP_FILES:src/%.cpp=$(PROFILE)/%.o)
D_FILES = $(CPP_FILES:src/%.cpp=$(PROFILE)/%.d)
# LDFLAGS = -ldep1 -L/usr/share/library1
EXE_NAME = main

$(PROFILE)/$(EXE_NAME): $(OBJ_FILES) src/libjpeg-turbo/build/libjpeg.a src/libjpeg-turbo/build/libturbojpeg.a
	$(CXX) $^ -o $@ $(LDFLAGS)

$(PROFILE)/%.o: src/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $< -o $@

-include $(D_FILES)

.PHONY: clean distclean
clean: 
	rm -rf $(PROFILE) out.jpg

distclean:
	rm -rf Debug Release out.jpg