#include <iostream>
#include <string>
#include <jpeglib.h>
#include <turbojpeg.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>

const char *subsampName[TJ_NUMSAMP] = {
    "4:4:4", "4:2:2", "4:2:0", "Grayscale", "4:4:0", "4:1:1"
};

const char *colorspaceName[TJ_NUMCS] = {
    "RGB", "YCbCr", "GRAY", "CMYK", "YCCK"
};

static int invertingFilter(short *coeffs, tjregion arrayRegion,
                        tjregion planeRegion, int componentIndex,
                        int transformIndex, tjtransform *transform)
{   
    for (int i = 0; i < arrayRegion.w * arrayRegion.h; i++) {
        coeffs[i] = -coeffs[i];
    }

    return 0;
}

static int displayFilter(short *coeffs, tjregion arrayRegion,
                        tjregion planeRegion, int componentIndex,
                        int transformIndex, tjtransform *transform)
{   
    for (int i = 0; i < arrayRegion.w * arrayRegion.h; i++) {
        std::cout << coeffs[i] << " ";
    }

    return 0;
}

char *globalMessage;
bool written;
int writtenChars = 0;
        
static int stegoFilter(short *coeffs, tjregion arrayRegion,
                        tjregion planeRegion, int componentIndex,
                        int transformIndex, tjtransform *transform)
{
    if (!written) {
        char *msg = globalMessage;
        
        int writtenBits = 0;
        int k = 0;

        while (writtenChars < strlen(msg)) {
            writtenBits = 0;

            while (writtenBits < 8) {
                if (coeffs[k] == 1) {
                    coeffs[k] = 2;
                }

                if (coeffs[k] != 0 && coeffs[k] != 1) {
                    if (((msg[writtenChars] >> (7 - writtenBits)) & 1) == 1) {
                        coeffs[k] |= 1;
                        writtenBits++;
                    } else {
                        coeffs[k] &= ~1;
                        writtenBits++;
                    }
                } 

                k++;
            }

            // after 8 bits, one character is written            
            std::cout << "character written " << msg[writtenChars] << "\n";
            writtenChars++;            
        }

        written = true;
    }
    
    return 0;
}

char decodedMessage[100];
int messageLength;
bool read;
int readChars = 0;

static int destegoFilter(short *coeffs, tjregion arrayRegion,
                        tjregion planeRegion, int componentIndex,
                        int transformIndex, tjtransform *transform)
{    
    if (!read) {
        char character;        
        
        int readBits = 0;
        int k = 0;
        
        while (readChars < messageLength) {
            character = 0;
            readBits = 0;


            while (readBits < 8) {
                if (coeffs[k] != 0) {                    
                    character <<= 1;
                    character |= (coeffs[k] & 1);
                    readBits++;
                } 

                k++;
            }
            // after 8 bits, one character is written
            decodedMessage[readChars] = character;
            readChars++;            
        }
        read = true;
    }
    
    return 0;
}

void destego(char *srcImgName, int msglen) {
    // set globals
    read = false;
    messageLength = msglen;
    
    // jpeg buffers
    FILE *jpegFile = NULL;
    uint8_t *jpegBuf = NULL;    
    long size;
    unsigned long jpegSize;
    int flags = 0;

    //init transform
    tjtransform xform;
    memset(&xform, 0, sizeof(tjtransform));
    xform.op = TJXOP_NONE;
    xform.customFilter = destegoFilter;

    // Read carrier file
    if ((jpegFile = fopen(srcImgName, "rb")) == NULL) {
        std::cout << "error reading input jpeg file" << std::endl;
    }

    if (fseek(jpegFile, 0, SEEK_END) < 0 || ((size = ftell(jpegFile)) < 0) ||
        fseek(jpegFile, 0, SEEK_SET) < 0) {
        std::cout << "error determining input file size" << std::endl;
    }

    jpegSize = (unsigned long)size;

    if ((jpegBuf = (uint8_t *) tjAlloc(jpegSize)) == NULL) {
        std::cout << "error allocating JPEG buffer" << std::endl;
    }

    if ((fread(jpegBuf, jpegSize, 1, jpegFile)) == NULL) {
        std::cout << "error reading input file" << std::endl;
    }

    fclose(jpegFile);  jpegFile = NULL;

    // Transform
    tjhandle tjInstance = NULL;
    uint8_t *dstBuf = NULL;
    unsigned long dstSize = 0;

    if ((tjInstance = tjInitTransform()) == NULL) {
        std::cout << "error initializing transformer" << std::endl;
    }

    if (tjTransform(tjInstance, jpegBuf, jpegSize, 1, &dstBuf, &dstSize, &xform, flags) < 0) {
        tjFree(dstBuf);
        std::cout << "error transforming image" << std::endl;
    }

    tjFree(jpegBuf);
    jpegBuf = dstBuf;
    jpegSize = dstSize;

    if (read) {
        std::cout << "decoded message: " << decodedMessage << std::endl;
    }

    tjDestroy(tjInstance);  tjInstance = NULL;
    tjFree(jpegBuf);  jpegBuf = NULL; 

}

void stego(char *dstImgName, char *srcImgName, char *message) {
    // set globals
    globalMessage = message;
    written = false;
    
    // jpeg buffers
    FILE *jpegFile = NULL;
    uint8_t *jpegBuf = NULL;    
    long size;
    unsigned long jpegSize;
    int flags = 0;

    //init transform
    tjtransform xform;
    memset(&xform, 0, sizeof(tjtransform));
    xform.op = TJXOP_NONE;
    xform.customFilter = stegoFilter;
    // xform.customFilter = displayFilter;
    // xform.customFilter = invertingFilter;
    //xform.data = (void*)message;

    // Read carrier file
    if ((jpegFile = fopen(srcImgName, "rb")) == NULL) {
        std::cout << "error reading input jpeg file" << std::endl;
    }

    if (fseek(jpegFile, 0, SEEK_END) < 0 || ((size = ftell(jpegFile)) < 0) ||
        fseek(jpegFile, 0, SEEK_SET) < 0) {
        std::cout << "error determining input file size" << std::endl;
    }

    jpegSize = (unsigned long)size;

    if ((jpegBuf = (uint8_t *) tjAlloc(jpegSize)) == NULL) {
        std::cout << "error allocating JPEG buffer" << std::endl;
    }

    if ((fread(jpegBuf, jpegSize, 1, jpegFile)) == NULL) {
        std::cout << "error reading input file" << std::endl;
    }

    fclose(jpegFile);  jpegFile = NULL;

    // Transform
    tjhandle tjInstance = NULL;
    uint8_t *dstBuf = NULL;
    unsigned long dstSize = 0;

    if ((tjInstance = tjInitTransform()) == NULL) {
        std::cout << "error initializing transformer" << std::endl;
    }

    if (tjTransform(tjInstance, jpegBuf, jpegSize, 1, &dstBuf, &dstSize, &xform, flags) < 0) {
        tjFree(dstBuf);
        std::cout << "error transforming image" << std::endl;
    }

    tjFree(jpegBuf);
    jpegBuf = dstBuf;
    jpegSize = dstSize;

    // test generated image header
    int inSubsamp, inColorspace;
    int width, height;

    if (tjDecompressHeader3(tjInstance, jpegBuf, jpegSize, &width, &height,
                            &inSubsamp, &inColorspace) < 0){
        std::cout << "error reading JPEG header" << std::endl;
    }

    printf("%s Image:  %d x %d pixels, %s subsampling, %s colorspace\n",
           "Transformed", width, height,
           subsampName[inSubsamp], colorspaceName[inColorspace]);

    // Write transformed image
    
    if ((jpegFile = fopen(dstImgName, "wb")) == NULL) {
        std::cout << "error opening output file" << std::endl;
    } else {
        std::cout << "output file opened" << std::endl;
    }
    if (fwrite(jpegBuf, jpegSize, 1, jpegFile) < 1) {
        std::cout << "error writing output file" << std::endl;
    } else {
        std::cout << "written output file" << std::endl;
    }
    tjDestroy(tjInstance);  tjInstance = NULL;
    fclose(jpegFile);  jpegFile = NULL;
    tjFree(jpegBuf);  jpegBuf = NULL; 

}

void usage(char *programName) {
    std::cout << "\nUSAGE: " << programName << " stego <destination_jpg_file> <carrier_jpg_file> <message>" << std::endl
              << "OR: " << programName << " destego <src_jpg_file> <message_length>" << std::endl;
}

int main(int argc, char **argv) {
    std::cout << argc << std::endl;

    if (argc == 5 && strcmp(argv[1], "stego") == 0) {
        stego(argv[2], argv[3], argv[4]);
        // stego("out.jpg", "carrier.jpeg", msg);
    } else if (argc == 4 && strcmp(argv[1], "destego") == 0) {
        // destego("out.jpg", strlen(msg));
        destego(argv[2], atoi(argv[3]));
    } else if (argc == 2 && strcmp(argv[1], "test") == 0) {
        char msg[] = "hello, aces";
        stego("out.jpg", "carrier.jpeg", msg);
        destego("out.jpg", strlen(msg));
    } else {
        usage(argv[0]);
    }
    
    return 0;
}